<?php
use App\Http\Controllers\AboutController;
use App\Http\Controllers\AgencyController;
use App\Http\Controllers\AgentController;
use App\Http\Controllers\ContactUsController;
use App\Http\Controllers\DetailController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\HouseController;
use App\Http\Controllers\PlotController;
use App\Models\CategoryType;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    $types=CategoryType::all();
//
//    return view('khonike.welcome',compact('types'));
//})->name('welcome');
Route::get('/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    Artisan::call('config:cache');
    return 'cleared';
});
Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('welcome');
Route::post('/search-property', [HomeController::class, 'property'])->name('search-property');


//house routes
Route::get('/house', [HouseController::class, 'index'])->name('houses');

// agency routes
Route::get('/agency', [AgencyController::class, 'index'])->name('agency');

// agency details
Route::get('/detail', [DetailController::class, 'index'])->name('detail');

// about details
//Route::get('/about', [AboutController::class, 'index'])->name('about');

// agent details
Route::get('/agent', [AgentController::class, 'index'])->name('agent');

//for plot admin
Route::get('/plot', [PlotController::class, 'index'])->name('plot');

//for contact us
Route::get('/contact', [ContactUsController::class, 'index'])->name('contact');

//for contact us
Route::get('/property-details/{id}', [GalleryController::class, 'index'])->name('property-details');

Route::group(['prefix' => 'admin'], function () {
    Route::get('/about', [AboutController::class, 'index'])->name('about');

//    Route::get('aboutus',[AboutController::class, 'index'])->name('aboutus');
    Voyager::routes();
});
