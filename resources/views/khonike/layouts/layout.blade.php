<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Khonike</title>

    <link href="{{asset('assets/images/favicon.ico')}}" type="img/x-icon" rel="shortcut icon">
    <!-- All css files are included here. -->
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/iconfont.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/plugins.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/helper.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
    <!-- Modernizr JS -->
    <script src="{{URL::asset('jss/vendor/modernizr-2.8.3.min.js')}}"></script>
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">--}}
</head>
<body>


<header class="header header-sticky">
    <div class="header-bottom menu-center">
        <div class="container">
            <div class="row justify-content-between">

                <!--Logo start-->
                <div class="col mt-10 mb-10">
                    <div class="logo">
                        <a href="{{ROUTE('welcome')}}"><img src="{{asset('assets/images/logo.png')}}" alt=""></a>
                    </div>
                </div>
                <!--Logo end-->

                <!--Menu start-->
                <div class="col d-none d-lg-flex">
                    <nav class="main-menu">
                        <ul>
                            <li class=""><a href="{{route('welcome')}}">Home</a>
{{--                                <ul class="sub-menu">--}}
{{--                                    <li><a href="index.html">Home one</a></li>--}}
{{--                                    <li><a href="index-2.html">Home two</a></li>--}}
{{--                                    <li><a href="index-3.html">Home three</a></li>--}}
{{--                                </ul>--}}
                            </li>
                            <li class="has-dropdown"><a href="javascript:void(0)">Categories</a>
                                <ul class="sub-menu">
                                    <li class=""><a href="javascript:void(0)">Plot</a>
{{--                                        <ul class="sub-menu">--}}
{{--                                            <li><a href="properties.html">Default Layout</a></li>--}}
{{--                                            <li><a href="properties-left-sidebar.html">Left Sidebar</a></li>--}}
{{--                                            <li><a href="properties-right-sidebar.html">Right Sidebar</a></li>--}}
{{--                                        </ul>--}}
                                    </li>
                                    <li class=""><a href="{{route('houses')}}">Houses</a>
{{--                                        <ul class="sub-menu">--}}
{{--                                            <li><a href="properties-list-left-sidebar.html">Left Sidebar</a></li>--}}
{{--                                            <li><a href="properties-list-right-sidebar.html">Right Sidebar</a></li>--}}
{{--                                        </ul>--}}
                                    </li>
{{--                                    <li class=""><a href="#">Rental Houses</a>--}}
{{--                                        <ul class="sub-menu">--}}
{{--                                            <li><a href="properties-carousel.html">Carousel Single Row</a></li>--}}
{{--                                            <li><a href="properties-carousel2.html">Carousel Double Row</a></li>--}}
{{--                                        </ul>--}}
{{--                                    </li>--}}
{{--                                    <li class="has-dropdown"><a href="#">Commercial</a>--}}
{{--                                        <ul class="sub-menu">--}}
{{--                                            <li><a href="#">Plaza</a></li>--}}
{{--                                            <li><a href="#">Building</a></li>--}}
{{--                                            <li><a href="#">Flate</a></li>--}}
{{--                                            <li><a href="#">Offices</a></li>--}}
{{--                                            <li><a href="#">Shops</a>--}}
{{--                                            <li><a href="#">Ohers</a></li>--}}



{{--                                        </ul>--}}
{{--                                    </li>--}}
                                </ul>
                            </li>
{{--                            <li class="has-dropdown"><a href="agent.html">agents</a>--}}
{{--                                <ul class="sub-menu">--}}
{{--                                    <li class="has-dropdown"><a href="agents.html">Agents Grid</a>--}}
{{--                                        <ul class="sub-menu">--}}
{{--                                            <li><a href="agents-3-column.html">Three Column</a></li>--}}
{{--                                            <li><a href="agents.html">Four Column</a></li>--}}
{{--                                        </ul>--}}
{{--                                    </li>--}}
{{--                                    <li class="has-dropdown"><a href="agents-carousel-3-column.html">Agents Carousel</a>--}}
{{--                                        <ul class="sub-menu">--}}
{{--                                            <li><a href="agents-carousel-3-column.html">Three Column</a></li>--}}
{{--                                            <li><a href="agents-carousel2-3-column.html">Three Column Double Row</a></li>--}}
{{--                                            <li><a href="agents-carousel-4-column.html">Four Column</a></li>--}}
{{--                                            <li><a href="agents-carousel2-4-column.html">Four Column Double Row</a></li>--}}
{{--                                        </ul>--}}
{{--                                    </li>--}}
{{--                                    <li><a href="#">Agent Details</a></li>--}}
{{--                                </ul>--}}
{{--                            </li>--}}
{{--                            <li class="has-dropdown"><a href="{{route('agency')}}">Agencies</a>--}}
{{--                                <ul class="sub-menu">--}}
{{--                                    <li><a href="{{route('agency')}}">Agencies</a></li>--}}
{{--                                    <li><a href="{{route('detail')}}">Agencies Details</a></li>--}}
{{--                                </ul>--}}
{{--                            </li>--}}
                            <li class=""><a href="{{route('about')}}">About us</a>
                            <li class=""><a href="{{route('contact')}}">Contact us</a>
{{--                                <ul class="sub-menu">--}}
{{--                                    <li><a href="about-us.html">About us</a></li>--}}
{{--                                    <li><a href="add-properties.html">Add Properties</a></li>--}}
{{--                                    <li><a href="contact-us.html">Contact us</a></li>--}}
{{--                                    <li><a href="gallery-2-column.html">Gallery 2 Column</a></li>--}}
{{--                                    <li><a href="gallery-3-column.html">Gallery 3 Column</a></li>--}}
{{--                                    <li><a href="gallery-4-column.html">Gallery 4 Column</a></li>--}}
{{--                                    <li><a href="login-register.html">Login & Register</a></li>--}}
{{--                                    <li><a href="my-account.html">My Account</a></li>--}}
{{--                                </ul>--}}
                            </li>
                        </ul>
                    </nav>
                </div>
                <!--Menu end-->

                <!--User start-->
                <div class="col mr-sm-50 mr-xs-50">
                    <div class="header-user">
{{--                        <a href="{{ route('login') }}" class="user-toggle"><i class="pe-7s-user"></i><span>Login or Register</span></a>--}}
                    </div>
                </div>
                <!--User end-->
            </div>

            <!--Mobile Menu start-->
            <div class="row">
                <div class="col-12 d-flex d-lg-none">
                    <div class="mobile-menu"></div>
                </div>
            </div>
            <!--Mobile Menu end-->

        </div>
    </div>
</header>

@yield('content')




<footer class="footer-section section" style="background-image: url({{asset('assets/images/bg/footer-bg.jpg')}})">

    <!--Footer Top start-->
    <div class="footer-top section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-60 pb-lg-40 pb-md-30 pb-sm-20 pb-xs-10">
        <div class="container">
            <div class="row row-25">

                <!--Footer Widget start-->
                <div class="footer-widget col-lg-3 col-md-6 col-12 mb-40">
                    <img src="{{asset('assets/images/logo-footer.png')}}" alt="">
                    <p>Propertvillage.com - Pakistan's best free property listings website. Trusted place to investing, renting, buying & selling online.</p>
                    <div class="footer-social">
                        <a href="javascript:void(0)" class="facebook"><i class="fa fa-facebook"></i></a>
                        <a href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=villageproperty12345@gmail.com" target="_blank" class="google"><i class="fa fa-google-plus"></i></a>
                    </div>
                </div>
                <!--Footer Widget end-->


                <!--Footer Widget start-->
                <div class="footer-widget col-lg-3 col-md-6 col-12 mb-40">
                    <h4 class="title"><span class="text">Contact</span><span class="shape"></span></h4>
                    <ul>
                        <li><i class="fa fa-map-o"></i><span>214, Capital Business, Center, 2nd Floor, F-10 Markaz Islamabad</span></li>
                        <li><i class="fa fa-phone"></i><span><a href="javascript:void(0)">0334-4440350</a><a href="javascript:void(0)">051-8748618</a></span></li>
                        <li><i class="fa fa-envelope-o"></i><span><a href="javascript:void(0)">Villageproperty12345@gmail.com</a></span></li>
                    </ul>
                </div>
                <!--Footer Widget end-->
                <!--Footer Widget start-->
                <div class="footer-widget col-lg-3 col-md-6 col-12 mb-40">
                                        <h4 class="title"><span class="text">Quick Links</span><span class="shape"></span></h4>
                                        <ul>
                                            <li><a href="{{route('about')}}">About Us</a></li>
                                            <li><a href="{{route('contact')}}">Contact Us</a></li>
{{--                                            <li><a href="#">Top Mortagages Rates</a></li>--}}
{{--                                            <li><a href="#">RentalTerms of use</a></li>--}}
{{--                                            <li><a href="#">Privacy Policy</a></li>--}}
                                        </ul>
                </div>
                <!--Footer Widget end-->


                <!--Footer Widget start-->
                <div class="footer-widget col-lg-3 col-md-6 col-12 mb-40">
                    <h4 class="title"><span class="text">Newsletter</span><span class="shape"></span></h4>

                    <p>Get all latest news about our latest properties, promotions, offers and discount</p>

                    <form id="mc-form" class="mc-form footer-newsletter" >
                        <input id="mc-email" type="email" autocomplete="off" placeholder="Email Here.." />
                        <button id="mc-submit" disabled><i class="fa fa-paper-plane-o"></i></button>
                    </form>
                    <!-- mailchimp-alerts Start -->
                    <div class="mailchimp-alerts text-centre">
                        <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                        <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                        <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                    </div><!-- mailchimp-alerts end -->

                </div>
                <!--Footer Widget end-->

            </div>
        </div>
    </div>
    <!--Footer Top end-->

    <!--Footer bottom start-->
    <div class="footer-bottom section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="copyright text-center">
                        <p>Copyright &copy;2022 <a  href="javascript:void(0)">Property Village</a>. All rights reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Footer bottom end-->

</footer>

<!-- Placed js at the end of the document so the pages load faster -->

<!-- All jquery file included here -->
<script src="https://maps.google.com/maps/api/js?sensor=false&amp;libraries=geometry&amp;v=3.22&amp;key=AIzaSyDAq7MrCR1A2qIShmjbtLHSKjcEIEBEEwM"></script>
<script src="{{URL::asset('jss/vendor/jquery-1.12.4.min.js')}}"></script>
<script src="{{URL::asset('jss/vendor/jquery-migrate-1.4.1.min.js')}}"></script>
<script src="{{URL::asset('jss/popper.min.js')}}"></script>
<script src="{{URL::asset('jss/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('jss/plugins.js')}}"></script>
<script src="{{URL::asset('jss/map-place.js')}}"></script>
<script src="{{URL::asset('jss/main.js')}}"></script>
@yield('script')
</body>
</html>

