@extends('khonike.layouts.layout')

@section('content')
    <!--Mobile Menu start-->
    <div class="row">
        <div class="col-12 d-flex d-lg-none">
            <div class="mobile-menu"></div>
        </div>
    </div>
    <!--Mobile Menu end-->

    </div>
    </div>
    </header>
    <!--Header section end-->

    <!--Page Banner Section start-->
    <div class="page-banner-section section">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1 class="page-banner-title">About us</h1>
                    <ul class="page-breadcrumb">
                        <li><a href="{{route('welcome')}}">Home</a></li>
                        <li class="active">About us</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Page Banner Section end-->

    <!--Welcome Khonike - Real Estate Bootstrap 4 Templatesection-->
    <div
        class="feature-section feature-section-border-top section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-60 pb-lg-40 pb-md-30 pb-sm-20 pb-xs-10">
        <div class="container">
            <div class="row row-25 align-items-center">

                <!--Feature Image start-->
                <div class="col-lg-5 col-12 order-1 order-lg-2 mb-40">
                    <div class="feature-image"><img src="{{asset('assets/images/property/property-6.jpg')}}" alt="">
                    </div>
                </div>
                <!--Feature Image end-->

                <div class="col-lg-7 col-12 order-2 order-lg-1 mb-40">

                    <div class="row">
                        <div class="col">
                            <div class="about-content">
                                <h3>Welcome to <span>Property Village</span></h3>
                                <h1>We have a buyer for every property</h1>
                                <p>List your properties and sell to more than 2 million buyers that visit property
                                    village.com every month</p>

                                <ul class="feature-list">
                                    <li>
                                        <i class="pe-7s-piggy"></i>
                                        <h4>Low Cost</h4>
                                    </li>
                                    <li>
                                        <i class="pe-7s-science"></i>
                                        <h4>Modern Design</h4>
                                    </li>
                                    <li>
                                        <i class="pe-7s-display1"></i>
                                        <h4>Good Marketing</h4>
                                    </li>
                                    <li>
                                        <i class="pe-7s-signal"></i>
                                        <h4>Free Wifi</h4>
                                    </li>
                                    <li>
                                        <i class="pe-7s-map"></i>
                                        <h4>Easy to Find</h4>
                                    </li>
                                    <li>
                                        <i class="pe-7s-shield"></i>
                                        <h4>Reliable</h4>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <!--Welcome Khonike - Real Estate Bootstrap 4 Templatesection end-->

    <!--Download apps section start-->
    <div class="download-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50"
         style="background-image: url({{asset('assets/images/bg/download-bg.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-12">

                    <!--Download Content start-->
                    <div class="download-content">
                        <h1>Pakistan's 1st Online Real Estate Marketplace
                        </h1>
                        <div class="buttons">
                            <a href="javascript:void(0)">
                                <i class="fa fa-apple"></i>
                                <span class="text">
                                    <span>Available on the</span>
                                    Apple Store
                                </span>
                            </a>
                            <a href="javascript:void(0)">
                                <i class="fa fa-android"></i>
                                <span class="text">
                                    <span>Get it on</span>
                                    Google Play
                                </span>
                            </a>
                            <a href="javascript:void(0)">
                                <i class="fa fa-windows"></i>
                                <span class="text">
                                    <span>Download form</span>
                                    Windows Store
                                </span>
                            </a>
                        </div>
                        <div class="image"><img src="{{asset('assets/images/others/app.png')}}" alt=""></div>
                    </div>
                    <!--Download Content end-->

                </div>
            </div>
        </div>
    </div>
    <!--Download apps section end-->

    <!--Services section start-->
    <div
        class="service-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-70 pb-lg-50 pb-md-40 pb-sm-30 pb-xs-20">
        <div class="container">

            <!--Section Title start-->
            <div class="row">
                <div class="col-md-12 mb-60 mb-xs-30">
                    <div class="section-title center">
                        <h1>Our Services</h1>
                    </div>
                </div>
            </div>
            <!--Section Title end-->

            <div class="row row-30 align-items-center">
                <div class="col-lg-5 col-12 mb-30">
                    <div class="property-slider-2">
                        <div class="property-2">
                            <div class="property-inner">
                                <a href="javascript:void(0)" class="image"><img
                                        src="{{asset('assets/images/property/property-13.jpg')}}" alt=""></a>
                                {{--                                <div class="content">--}}
                                {{--                                    <h4 class="title"><a href="single-properties.html">Friuli-Venezia Giulia</a></h4>--}}
                                {{--                                    <span class="location">568 E 1st Ave, Miami</span>--}}
                                {{--                                    <h4 class="type">Rent <span>$550 <span>Month</span></span></h4>--}}
                                {{--                                    <ul>--}}
                                {{--                                        <li>6 Bed</li>--}}
                                {{--                                        <li>4 Bath</li>--}}
                                {{--                                        <li>3 Garage</li>--}}
                                {{--                                    </ul>--}}
                                {{--                                </div>--}}
                            </div>
                        </div>
                        <div class="property-2">
                            <div class="property-inner">
                                <a href="javascript:void(0)" class="image"><img
                                        src="{{asset('assets/images/property/property-14.jpg')}}" alt=""></a>
                                {{--                                <div class="content">--}}
                                {{--                                    <h4 class="title"><a href="single-properties.html">Marvel de Villa</a></h4>--}}
                                {{--                                    <span class="location">450 E 1st Ave, New Jersey</span>--}}
                                {{--                                    <h4 class="type">Rent <span>$550 <span>Month</span></span></h4>--}}
                                {{--                                    <ul>--}}
                                {{--                                        <li>6 Bed</li>--}}
                                {{--                                        <li>4 Bath</li>--}}
                                {{--                                        <li>3 Garage</li>--}}
                                {{--                                    </ul>--}}
                                {{--                                </div>--}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-12">
                    <div class="row row-20">

                        <!--Service start-->
                        <div class="col-md-6 col-12 mb-30">
                            <div class="service">
                                <div class="service-inner">
                                    <div class="head">
                                        <div class="icon"><img src="{{asset('assets/images/service/service-1.png')}}"
                                                               alt=""></div>
                                        <h4>BUYING</h4>
                                    </div>
                                    <div class="content">
                                        <p>Share your budget and property requirements and we will ensure you seemlesss
                                            transactions at best possible prices according to your wishes.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Service end-->

                        <!--Service start-->
                        <div class="col-md-6 col-12 mb-30">
                            <div class="service">
                                <div class="service-inner">
                                    <div class="head">
                                        <div class="icon"><img src="{{asset('assets/images/service/service-2.png')}}"
                                                               alt=""></div>
                                        <h4>SELLING</h4>
                                    </div>
                                    <div class="content">
                                        <p>Selling properties through Propert Village Real Estate will ensure that your
                                            porpose is served properly. We are best in locating suitable buyers without
                                            time lapse.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Service end-->

                        <!--Service start-->
                        <div class="col-md-6 col-12 mb-30">
                            <div class="service">
                                <div class="service-inner">
                                    <div class="head">
                                        <div class="icon"><img src="{{asset('assets/images/service/service-3.png')}}"
                                                               alt=""></div>
                                        <h4>Rent Property</h4>
                                    </div>
                                    <div class="content">
                                        <p>Rent properties through Propert Village Real Estate will ensure that your
                                            porpose is served properly.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Service end-->

                        <!--Service start-->
                        <div class="col-md-6 col-12 mb-30">
                            <div class="service">
                                <div class="service-inner">
                                    <div class="head">
                                        <div class="icon"><img src="{{asset('assets/images/service/service-4.png')}}"
                                                               alt=""></div>
                                        <h4>CONSULTANCY</h4>
                                    </div>
                                    <div class="content">
                                        <p>Contact our professionally trained consultants at Property Village Real
                                            Estate for consultation at your convenience.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Service end-->

                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--Services section end-->

    <!--Funfact Section start-->
    <div
        class="funfact-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-70 pb-lg-50 pb-md-40 pb-sm-30 pb-xs-20"
        style="background-image: url({{url('assets/images/bg/cta-bg.jpg')}})">
        <div class="container">
            <div class="row">

                <!--Funfact start-->
                <div class="single-fact col-lg-3 col-6 mb-30">
                    <div class="inner">
                        <div class="head">
                            <i class="pe-7s-home"></i>
                            <h3 class="counter">854</h3>
                        </div>
                        <p>Complete Project</p>
                    </div>
                </div>
                <!--Funfact end-->

                <!--Funfact start-->
                <div class="single-fact col-lg-3 col-6 mb-30">
                    <div class="inner">
                        <div class="head">
                            <i class="pe-7s-graph3"></i>
                            <h3 class="counter">854</h3>
                        </div>
                        <p>Property Sold</p>
                    </div>
                </div>
                <!--Funfact end-->

                <!--Funfact start-->
                <div class="single-fact col-lg-3 col-6 mb-30">
                    <div class="inner">
                        <div class="head">
                            <i class="pe-7s-users"></i>
                            <h3 class="counter">854</h3>
                        </div>
                        <p>Happy Clients</p>
                    </div>
                </div>
                <!--Funfact end-->

                <!--Funfact start-->
                <div class="single-fact col-lg-3 col-6 mb-30">
                    <div class="inner">
                        <div class="head">
                            <i class="pe-7s-medal"></i>
                            <h3 class="counter">854</h3>
                        </div>
                        <p>Awards Win</p>
                    </div>
                </div>
                <!--Funfact end-->

            </div>
        </div>
    </div>
    <!--Funfact Section end-->



    <!--Testimonial Section start-->
    <div
        class="testimonial-section section bg-gray pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-100 pb-lg-80 pb-md-70 pb-sm-60 pb-xs-50">
        <div class="container">

            <!--Section Title start-->
            <div class="row">
                <div class="col-md-12 mb-60 mb-xs-30">
                    <div class="section-title center">
                        <h1>What Client's Say</h1>
                    </div>
                </div>
            </div>
            <!--Section Title end-->

            <div class="row">

                <!--Review start-->
                <div class="review-slider section">

                    <div class="review col">
                        <div class="review-inner">
                            <div class="image"><img src="{{asset('assets/images/review/review-1.jpg')}}" alt=""></div>
                            <div class="content">
                                <p>We are so grateful to have been recommended the services of All About Real
                                    Estate.</p>
                                <h6>Patrick, Bec and Hazel, Vendors <span>February 2019</span></h6>
                            </div>
                        </div>
                    </div>

                    <div class="review col">
                        <div class="review-inner">
                            <div class="image"><img src="{{asset('assets/images/review/review-2.jpg')}}" alt=""></div>
                            <div class="content">
                                <p>We are so grateful to have been recommended the services of All About Real
                                    Estate.</p>
                                <h6>Daria, Purchased <span>July 2018</span></h6></div>
                        </div>
                    </div>

                    <div class="review col">
                        <div class="review-inner">
                            <div class="image"><img src="{{asset('assets/images/review/review-3.jpg')}}" alt=""></div>
                            <div class="content">
                                <p>We are so grateful to have been recommended the services of All About Real
                                    Estate.</p>
                                <h6>Angela & Steve, Purchased <span>February 2018</span></h6></div>
                        </div>
                    </div>

                    <div class="review col">
                        <div class="review-inner">
                            <div class="image"><img src="{{asset('assets/images/review/review-4.jpg')}}" alt=""></div>
                            <div class="content">
                                <p>We are so grateful to have been recommended the services of All About Real
                                    Estate.</p>
                                <h6>Matt & Sam, Vendors and purchasers<span></span></h6></div>
                        </div>
                    </div>

                    <div class="review col">
                        <div class="review-inner">
                            <div class="image"><img src="{{asset('assets/images/review/review-5.jpg')}}" alt=""></div>
                            <div class="content">
                                <p>We are so grateful to have been recommended the services of All About Real
                                    Estate.</p>
                                <h6>Kath, Jason, Maz and Pat, Buyers <span</span></h6></div>
                        </div>
                    </div>

                    <div class="review col">
                        <div class="review-inner">
                            <div class="image"><img src="{{asset('assets/images/review/review-6.jpg')}}" alt=""></div>
                            <div class="content">
                                <p>We are so grateful to have been recommended the services of All About Real
                                    Estate.</p>
                                <h6>Glenn & Karen, Vendors in Woodroff <span>February 2017</span></h6></div>
                        </div>
                    </div>

                </div>
                <!--Review end-->

            </div>
        </div>
    </div>
    <!--Testimonial Section end-->

    <!--Brand section start-->
    <div
        class="brand-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-100 pb-lg-80 pb-md-70 pb-sm-60 pb-xs-50">
        <div class="container">

            <!--Section Title start-->
            <div class="row">
                <div class="col-md-12 mb-60 mb-xs-30">
                    <div class="section-title center">
                        <h1>Our Partners</h1>
                    </div>
                </div>
            </div>
            <!--Section Title end-->

            <div class="row">
                <!--Brand Slider start-->
                <div class="brand-carousel section">
                    @foreach($partners as $partner)
                        <div class="brand col"><img src="{{asset('storage/'.$partner->image)}}" alt=""></div>
                    @endforeach
                </div>
                <!--Brand Slider end-->
            </div>

        </div>
    </div>
    <!--Brand section end-->
@endsection
