@extends('khonike.layouts.layout')
@section('content')

    <div id="main-wrapper">


        <!--Header section end-->

        <!--slider section start-->
        <div class="hero-section section position-relative">

            <!--Hero Item start-->
            <div class="hero-item player"
                 data-property="{videoURL:'FlME5z9tmLM',mobileFallbackImage:'images/bg/video-bg.jpg',coverImage:'assets/images/bg/video-bg.jpg',containment:'.player',startAt:0,mute:true,autoPlay:true,showControls:false,loop:true,opacity:1}">
                <div class="container">
                    <div class="row">
                        <div class="col-12">

                            <!--Hero Content start-->
                            <div class="hero-content">

                                <h3>WANT TO BUY OR RENT HOME ?</h3>
                                <h1><span>Property Village</span> Solve Your Problems</h1>

                            </div>
                            <!--Hero Content end-->

                        </div>
                    </div>
                </div>
            </div>
            <!--Hero Item end-->

        </div>
        <!--slider section end-->

        <!--Search Section start-->
        <div class="search-section section pt-0 pt-sm-60 pt-xs-50 ">
            <div class="container">

                <!--Section Title start-->
                <div class="row d-flex d-lg-none">
                    <div class="col-md-12 mb-60 mb-xs-30">
                        <div class="section-title center">
                            <h1>Find Your Home</h1>
                        </div>
                    </div>
                </div>
                <!--Section Title end-->

                <div class="row">
                    <div class="col-12">

                        <!--Hero Search start-->
                        <div class="hero-search">

                            <form action="{{route('search-property')}}" method="post">
                                @csrf
                                <div>
                                    <h4>Status</h4>
                                    <select class="nice-select" name="for_rent">
                                        <option
                                            {{ ( isset($searchedArray) && $searchedArray['for_rent'] == 1 ) ? 'selected' : ''}} value="1">
                                            For Rent
                                        </option>
                                        <option
                                            {{ (  isset($searchedArray) && $searchedArray['for_rent'] != 1 ) ? 'selected' : ''}} value="0">
                                            For Sale
                                        </option>
                                    </select>
                                </div>
                                <div>
                                    <h4>Type</h4>
                                    <select class="nice-select" name="type">
                                        @foreach($types as $type)

                                            <option
                                                {{ (  isset($searchedArray) && $searchedArray['type'] == $type->id ) ? 'selected' : ''}} value="{{$type->id}}">{{$type->name}}</option>

                                        @endforeach
                                    </select>


                                </div>

                                <div>
                                    <h4>Sub Category</h4>
                                    <select class="nice-select" id="sub_category" name="sub_category">
                                        @foreach($subCategory as $subCategories)
                                            {{--                                {{dd($subCategories->title)}};--}}
                                            <option
                                                {{ (  isset($searchedArray['sub_category']) && $searchedArray['sub_category'] == $subCategories->id ) ? 'selected' : ''}} value="{{$subCategories->id}}">{{$subCategories->title}}</option>

                                        @endforeach
                                    </select>


                                </div>

                                <div>
                                    <h4>Location</h4>

                                    <select class="nice-select" name="city">
                                        @foreach($cities as $city)

                                            <option
                                                {{  (isset($searchedArray) && $searchedArray['city'] == $city->id ) ? 'selected' : ''}} value="{{$city->id}}">{{$city->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                {{--                            <div>--}}
                                {{--                                <h4>Bedrooms</h4>--}}
                                {{--                                <select class="nice-select">--}}
                                {{--                                    <option>1</option>--}}
                                {{--                                    <option>2</option>--}}
                                {{--                                    <option>3</option>--}}
                                {{--                                    <option>4</option>--}}
                                {{--                                    <option>5</option>--}}
                                {{--                                    <option>6</option>--}}
                                {{--                                </select>--}}
                                {{--                            </div>--}}

                                {{--                            <div>--}}
                                {{--                                <h4>Bathrooms</h4>--}}
                                {{--                                <select class="nice-select">--}}
                                {{--                                    <option>1</option>--}}
                                {{--                                    <option>2</option>--}}
                                {{--                                    <option>3</option>--}}
                                {{--                                    <option>4</option>--}}
                                {{--                                    <option>5</option>--}}
                                {{--                                    <option>6</option>--}}
                                {{--                                </select>--}}
                                {{--                            </div>--}}

                                <div>
                                    <h4>Search</h4>
                                    <div class="submit">
                                        <button>click here</button>
                                    </div>
                                </div>

                            </form>

                        </div>
                        <!--Hero Search end-->

                    </div>
                </div>

            </div>
        </div>
        <!--Search Section end-->

        <!--New property section start-->
        <div
            class="property-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-60 pb-lg-40 pb-md-30 pb-sm-20 pb-xs-10">
            <div class="container">

                <!--Section Title start-->
                <div class="row">
                    <div class="col-md-12 mb-60 mb-xs-30">
                        <div class="section-title center">
                            <h1>Properties</h1>
                        </div>
                    </div>
                </div>
                <!--Section Title end-->

                <div class="row">

                    @foreach($properties as $property)
                        @php

                            $propertyImage = json_decode($property->images)[0];
                        @endphp

                            <!--Property start-->
                        <div class="property-item col-lg-4 col-md-6 col-12 mb-40">
                            <div class="property-inner">
                                <div class="image">
                                    <a href="{{route('property-details',$property->id)}}"><img
                                            src="{{asset('storage/'.$propertyImage)}}" alt="" height="350px"
                                            width="200px"></a>
                                    <ul class="property-feature">
                                        <li>
                                            <span class="area"><img src="{{asset('assets/images/icons/area.png')}}"
                                                                    alt="">{{$property->area}}SqFt</span>
                                        </li>
                                        <li>
                                            <span class="bed"><img src="{{asset('assets/images/icons/bed.png')}}"
                                                                   alt="">-</span>
                                        </li>
                                        <li>
                                            <span class="bath"><img src="{{asset('assets/images/icons/bath.png')}}"
                                                                    alt="">-</span>
                                        </li>
                                        <li>
                                            <span class="parking"><img
                                                    src="{{asset('assets/images/icons/parking.png')}}" alt="">-</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="content">
                                    <div class="left">
                                        <h3 class="title"><a href="javascript:void(0)">{{$property->name}}</a></h3>
                                        <span class="location"><img src="{{asset('assets/images/icons/marker.png')}}"
                                                                    alt="">{{$property->address}}</span>
                                    </div>
                                    <div class="right">
                                        <div class="type-wrap">
                                            <span class="price" style="font-size: smaller">PKR {{number_format( $property->price)}}<span>-</span></span>
                                            <span class="type">@if($property->for_rent==1)
                                                    for Rent
                                                @else
                                                    for sale
                        </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Property end-->

                    @endforeach


                    <!--Property end-->

                </div>
                <style>
                    a{
                        display: initial;
                    }
                </style>
                <span class="float-right">{{$properties->links()}}</span>

            </div>
        </div>
        <!--New property section end-->

        <!--Welcome Khonike - Real Estate Bootstrap 4 Templatesection-->
        <div
            class="feature-section feature-section-border-top section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-60 pb-lg-40 pb-md-30 pb-sm-20 pb-xs-10">
            <div class="container">
                <div class="row row-25 align-items-center">

                    <!--Feature Image start-->
                    <div class="col-lg-5 col-12 order-1 order-lg-2 mb-40">
                        <div class="feature-image"><img src="{{asset('assets/images/property/property-9.jpg')}}" alt="">
                        </div>
                    </div>
                    <!--Feature Image end-->

                    <div class="col-lg-7 col-12 order-2 order-lg-1 mb-40">
                        <div class="feature-wrap row row-25">

                            <!--Feature start-->
                            <div class="col-sm-6 col-12 mb-50">
                                <div class="feature">
                                    <div class="icon"><i class="pe-7s-piggy"></i></div>
                                    <div class="content">
                                        <h4>Low Cost</h4>
                                        <p>Are you planning to invest in the real estate market of Pakistan? If yes,
                                            then Property Village is best option.</p>
                                    </div>
                                </div>
                            </div>
                            <!--Feature end-->

                            <!--Feature start-->
                            <div class="col-sm-6 col-12 mb-50">
                                <div class="feature">
                                    <div class="icon"><i class="pe-7s-display1"></i></div>
                                    <div class="content">
                                        <h4>Good Marketing</h4>
                                        <p>The real estate industry desperately needs new tools and ideas to stay
                                            relevant to the modern consumer, and Property Village is one of them.</p>
                                    </div>
                                </div>
                            </div>
                            <!--Feature end-->

                            <!--Feature start-->
                            <div class="col-sm-6 col-12 mb-50">
                                <div class="feature">
                                    <div class="icon"><i class="pe-7s-map"></i></div>
                                    <div class="content">
                                        <h4>Easy to Find</h4>
                                        <p>Good real estates can manage the search for you by: staying on top of new
                                            listings, scheduling viewings and communicating with the agent representing
                                            the seller.</p>
                                    </div>
                                </div>
                            </div>
                            <!--Feature end-->

                            <!--Feature start-->
                            <div class="col-sm-6 col-12 mb-50">
                                <div class="feature">
                                    <div class="icon"><i class="pe-7s-shield"></i></div>
                                    <div class="content">
                                        <h4>Reliable</h4>
                                        <p>Our team of experts aim to deliver integrity and professionalism while
                                            helping you goals.</p>
                                    </div>
                                </div>
                            </div>
                            <!--Feature end-->

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!--Welcome Khonike - Real Estate Bootstrap 4 Templatesection end-->

        <!--Download apps section start-->
        <div class="download-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50"
             style="background-image: url({{asset('assets/images/bg/download-bg.jpg')}})">
            <div class="container">
                <div class="row">
                    <div class="col-12">

                        <!--Download Content start-->
                        <div class="download-content">
                            <h1>Pakistan's 1st Online Real Estate Marketplace</h1>
                            <div class="buttons">
                                <a href="javascript:void(0)">
                                    <i class="fa fa-apple"></i>
                                    <span class="text">
                                    <span>Available on the</span>
                                    Apple Store
                                </span>
                                </a>
                                <a href="javascript:void(0)">
                                    <i class="fa fa-android"></i>
                                    <span class="text">
                                    <span>Get it on</span>
                                    Google Play
                                </span>
                                </a>
                                <a href="javascript:void(0)">
                                    <i class="fa fa-windows"></i>
                                    <span class="text">
                                    <span>Download form</span>
                                    Windows Store
                                </span>
                                </a>
                            </div>
                            <div class="image"><img src="{{asset('assets/images/others/app.png')}}" alt=""></div>
                        </div>
                        <!--Download Content end-->

                    </div>
                </div>
            </div>
        </div>
        <!--Download apps section end-->

        <!--Services section start-->
        <div
            class="service-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-70 pb-lg-50 pb-md-40 pb-sm-30 pb-xs-20">
            <div class="container">

                <!--Section Title start-->
                <div class="row">
                    <div class="col-md-12 mb-60 mb-xs-30">
                        <div class="section-title center">
                            <h1>Our Services</h1>
                        </div>
                    </div>
                </div>
                <!--Section Title end-->

                <div class="row row-30 align-items-center">
                    <div class="col-lg-5 col-12 mb-30">
                        <div class="property-slider-2">
                            <div class="property-2">
                                <div class="property-inner">
                                    <a href="javascript:void(0)" class="image"><img
                                            src="{{asset('assets/images/property/property-13.jpg')}}" alt=""></a>
                                    {{--                                <div class="content">--}}
                                    {{--                                    <h4 class="title"><a href="single-properties.html">Friuli-Venezia Giulia</a></h4>--}}
                                    {{--                                    <span class="location">568 E 1st Ave, Miami</span>--}}
                                    {{--                                    <h4 class="type">Rent <span>$550 <span>Month</span></span></h4>--}}
                                    {{--                                    <ul>--}}
                                    {{--                                        <li>6 Bed</li>--}}
                                    {{--                                        <li>4 Bath</li>--}}
                                    {{--                                        <li>3 Garage</li>--}}
                                    {{--                                    </ul>--}}
                                    {{--                                </div>--}}
                                </div>
                            </div>
                            <div class="property-2">
                                <div class="property-inner">
                                    <a href="javascript:void(0)" class="image"><img
                                            src="{{asset('assets/images/property/property-14.jpg')}}" alt=""></a>
                                    {{--                                <div class="content">--}}
                                    {{--                                    <h4 class="title"><a href="single-properties.html">Marvel de Villa</a></h4>--}}
                                    {{--                                    <span class="location">450 E 1st Ave, New Jersey</span>--}}
                                    {{--                                    <h4 class="type">Rent <span>$550 <span>Month</span></span></h4>--}}
                                    {{--                                    <ul>--}}
                                    {{--                                        <li>6 Bed</li>--}}
                                    {{--                                        <li>4 Bath</li>--}}
                                    {{--                                        <li>3 Garage</li>--}}
                                    {{--                                    </ul>--}}
                                    {{--                                </div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 col-12">
                        <div class="row row-20">

                            <!--Service start-->
                            <div class="col-md-6 col-12 mb-30">
                                <div class="service">
                                    <div class="service-inner">
                                        <div class="head">
                                            <div class="icon"><img
                                                    src="{{asset('assets/images/service/service-1.png')}}" alt=""></div>
                                            <h4>BUYING</h4>
                                        </div>
                                        <div class="content">
                                            <p>Share your budget and property requirements and we will ensure you
                                                seemlesss
                                                transactions at best possible prices according to your wishes.</p></div>
                                    </div>
                                </div>
                            </div>
                            <!--Service end-->

                            <!--Service start-->
                            <div class="col-md-6 col-12 mb-30">
                                <div class="service">
                                    <div class="service-inner">
                                        <div class="head">
                                            <div class="icon"><img
                                                    src="{{asset('assets/images/service/service-2.png')}}"
                                                    alt=""></div>
                                            <h4>SELLING</h4>
                                        </div>
                                        <div class="content">
                                            <p>Selling properties through Propert Village Real Estate will ensure that
                                                your
                                                porpose is served properly. We are best in locating suitable buyers
                                                without
                                                time lapse.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Service end-->

                            <!--Service start-->
                            <div class="col-md-6 col-12 mb-30">
                                <div class="service">
                                    <div class="service-inner">
                                        <div class="head">
                                            <div class="icon"><img
                                                    src="{{asset('assets/images/service/service-3.png')}}"
                                                    alt=""></div>
                                            <h4>Rent Property</h4>
                                        </div>
                                        <div class="content">
                                            <p>Rent properties through Propert Village Real Estate will ensure that your
                                                porpose is served properly.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Service end-->

                            <!--Service start-->
                            <div class="col-md-6 col-12 mb-30">
                                <div class="service">
                                    <div class="service-inner">
                                        <div class="head">
                                            <div class="icon"><img
                                                    src="{{asset('assets/images/service/service-4.png')}}"
                                                    alt=""></div>
                                            <h4>CONSULTANCY</h4>
                                        </div>
                                        <div class="content">
                                            <p>Contact our professionally trained consultants at Property Village Real
                                                Estate for consultation at your convenience.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Service end-->

                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--Services section end-->

        <!--Feature property section start-->
        <div class="property-section section pb-100 pb-lg-80 pb-md-70 pb-sm-60 pb-xs-50">
            <div class="container">

                <!--Section Title start-->
                <div class="row">
                    <div class="col-md-12 mb-60 mb-xs-30">
                        <div class="section-title center">
                            <h1>Feature Property</h1>
                        </div>
                    </div>
                </div>
                <!--Section Title end-->

                <div class="row">

                    <!--Property Slider start-->
                    <div class="property-carousel section">
                        @foreach($properties as $property)
                            @php

                                $propertyImage = json_decode($property->images)[0];
                            @endphp
                                <!--Property start-->
                            <div class="property-item col">
                                <div class="property-inner">
                                    <div class="image">
                                        <a href="javascript:void(0)"><img src="{{asset('storage/'.$propertyImage)}}"
                                                                          alt="" height="350px" width="150px"></a>
                                        <ul class="property-feature">
                                            <li>
                                                <span class="area"><img src="{{asset('assets/images/icons/area.png')}}"
                                                                        alt="">{{$property->area}} SqFt</span>
                                            </li>
                                            <li>
                                                <span class="bed"><img src="{{asset('assets/images/icons/bed.png')}}"
                                                                       alt="">-</span>
                                            </li>
                                            <li>
                                                <span class="bath"><img src="{{asset('assets/images/icons/bath.png')}}"
                                                                        alt="">-</span>
                                            </li>
                                            <li>
                                                <span class="parking"><img
                                                        src="{{asset('assets/images/icons/parking.png')}}"
                                                        alt="">-</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="content">
                                        <div class="left">
                                            <h3 class="title"><a href="javascript:void(0)">{{$property->name}}</a></h3>
                                            <span class="location"><img
                                                    src="{{asset('assets/images/icons/marker.png')}}" alt="">{{$property->address}}</span>
                                        </div>
                                        <div class="right">
                                            <div class="type-wrap">
                                                <span class="price" style="font-size: smaller">PKR {{number_format($property->price)}}<span>-</span></span>
                                                <span class="type">@if($property->for_rent==1)
                                                        For Rent
                                                    @else
                                                        For Sale
                                                    @endif
                                        </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <!--Property end-->
                    </div>
                    <!--Property Slider end-->
                </div>
            </div>
        </div>
        <!--Feature property section end-->

        <!--CTA Section start-->
        <div
            class="cta-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-100 pb-lg-80 pb-md-70 pb-sm-60 pb-xs-50"
            style="background-image: url({{asset('assets/images/bg/cta-bg.jpg')}})">
            <div class="container">
                <div class="row">
                    <div class="col">

                        <!--CTA start-->
                        <div class="cta-content text-center">
                            <h1>Want to <span>Buy</span> New Property or <span>Sell</span> One <br> Do it in Seconds
                                With <span>Property Village</span></h1>
                            {{--                        <div class="buttons">--}}
                            {{--                            <a href="add-properties.html">Add Property</a>--}}
                            {{--                            <a href="properties.html">Browse Properties</a>--}}
                            {{--                        </div>--}}
                        </div>
                        <!--CTA end-->

                    </div>
                </div>
            </div>
        </div>
        <!--CTA Section end-->

        <!--Agent Section start-->
        <div
            class="agent-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-100 pb-lg-80 pb-md-70 pb-sm-60 pb-xs-50">
            <div class="container">

                <!--Section Title start-->
                <div class="row">
                    <div class="col-md-12 mb-60 mb-xs-30">
                        <div class="section-title center">
                            <h1>CEO'S</h1>
                        </div>
                    </div>
                </div>
                <!--Section Title end-->

                <div class="row">
                    <div class="agent-carousel section">

                        <!--Agent satrt-->
                        <div class="col">
                            <div class="agent">
                                <div class="image">
                                    <a class="img" href="javascript:void(0)"><img
                                            src="{{asset('assets/images/ceo/ceo1.jpeg')}}" alt="" height="350px"></a>
                                    <div class="social">
                                        <a href="https://www.facebook.com/uzair.mughal.35912" target="_blank"
                                           class="facebook"><i class="fa fa-facebook"></i></a>
                                        <a href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=villageproperty12345@gmail.com"
                                           target="_blank" class="google"><i class="fa fa-google-plus"></i></a>
                                    </div>
                                </div>
                                <div class="content">
                                    <h4 class="title"><a href="javascript:void(0)">Uzair Rafique</a></h4>
                                    <a href="javascript:void(0)" class="phone">0334 4440350</a>
                                    {{--                                <a href="javascript:void(0)" class="email">Villageproperty12345@gmial.com</a>--}}
                                    {{--                                <span class="properties">5 Properties</span>--}}
                                </div>
                            </div>
                        </div>
                        <!--Agent end-->

                        <!--Agent satrt-->
                        <div class="col">
                            <div class="agent">
                                <div class="image">
                                    <a class="img" href="javascript:void(0)"><img
                                            src="{{asset('assets/images/ceo/ceo2.jpeg')}}" alt="" height="350px"></a>
                                    <div class="social">
                                        <a href="javascript:void(0)" class="facebook"><i class="fa fa-facebook"></i></a>
                                        <a href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=ansarfarooqi458@gmail.com"
                                           target="_blank" class="google"><i class="fa fa-google-plus"></i></a>
                                    </div>
                                </div>
                                <div class="content">
                                    <h4 class="title"><a href="javascript:void(0)">Hafiz Ansar Mehmood </a></h4>
                                    <a href="javascript:void(0)" class="phone">0334 5634563</a>
                                    {{--                                <a href="javascript:void(0)" class="email">Villageproperty12345@gmial.com</a>--}}
                                    {{--                                <span class="properties">2 Properties</span>--}}
                                </div>
                            </div>
                        </div>
                        <!--Agent end-->

                        <!--Agent satrt-->
                        <div class="col">
                            <div class="agent">
                                <div class="image">
                                    <a class="img" href="javascript:void(0)"><img
                                            src="{{asset('assets/images/ceo/ceo3.jpeg')}}" alt="" height="350px"></a>
                                    <div class="social">
                                        <a href="javascript:void(0)" class="facebook"><i class="fa fa-facebook"></i></a>
                                        <a href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=villageproperty12345@gmail.com"
                                           target="_blank" class="google"><i class="fa fa-google-plus"></i></a>
                                    </div>
                                </div>
                                <div class="content">
                                    <h4 class="title"><a href="javascript:void(0)">Muhammad Shoaib</a></h4>
                                    <a href="javascript:void(0)" class="phone">0334 9650936</a>
                                    {{--                                <a href="javascript:void(0)" class="email">Villageproperty12345@gmial.com</a>--}}
                                    {{--                                <span class="properties">2 Properties</span>--}}
                                </div>
                            </div>
                        </div>
                        <!--Agent end-->


                    </div>
                </div>
            </div>
        </div>
        <!--Agent Section end-->

        <!--News Section start-->
        <div class="news-section section pb-100 pb-lg-80 pb-md-70 pb-sm-60 pb-xs-50">
            <div class="container">

                <!--Section Title start-->
                <div class="row">
                    <div class="col-md-12 mb-60 mb-xs-30">
                        <div class="section-title center">
                            <h1>Latest News</h1>
                        </div>
                    </div>
                </div>
                <!--Section Title end-->

                <div class="row">
                    <div class="news-carousel section">

                        <!--News start-->
                        <div class="col">
                            <div class="news">
                                <div class="image">
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/news/news-1.jpg')}}"
                                                                      alt=""></a>
                                    {{--                                <div class="meta-wrap">--}}
                                    {{--                                    <ul class="meta">--}}
                                    {{--                                        <li>By <a href="javascript:void(0)">Donald</a></li>--}}
                                    {{--                                        <li>September 30, 2018</li>--}}
                                    {{--                                    </ul>--}}
                                    {{--                                    <ul class="meta back">--}}
                                    {{--                                        <li>By <a href="javascript:void(0)">Donald</a></li>--}}
                                    {{--                                        <li>September 30, 2018</li>--}}
                                    {{--                                    </ul>--}}
                                    {{--                                </div>--}}
                                </div>
                                <div class="content">
                                    <h4 class="title"><a href="javascript:void(0)">Authorized Dealer</a></h4>
                                    <div class="desc">
                                        <p>FBR Certificate<br>Certificate From Islamabad Estate Agents
                                            Association(Membership Certificate) Awarded From I.L.H.S Society.</p>
                                    </div>
                                    {{--                                <a href="javascript:void(0)" class="readmore">Continure Reading</a>--}}
                                </div>
                            </div>
                        </div>
                        <!--News end-->

                        <!--News start-->
                        <div class="col">
                            <div class="news">
                                <div class="image">
                                    <a href="javascript:void(0)"><img src="assets/images/news/news-2.jpg" alt=""></a>

                                </div>
                                <div class="content">
                                    <h4 class="title"><a href="javascript:void(0)">Joint Mortgage: Pros vs. Cons</a>
                                    </h4>
                                    <div class="desc">
                                        <p>Property Village is ranked among the best real estate in pakistan. Such real
                                            estate projects have higher chances of timely completion and quality
                                            development.</p>
                                    </div>
                                    {{--                                <a href="news-details.html" class="readmore">Continure Reading</a>--}}
                                </div>
                            </div>
                        </div>
                        <!--News end-->

                        <!--News start-->
                        <div class="col">
                            <div class="news">
                                <div class="image">
                                    <a href="javascript:void(0)"><img src="assets/images/news/news-3.jpg" alt=""></a>

                                </div>
                                <div class="content">
                                    <h4 class="title"><a href="javascript:void(0)">Dealing with Student Loan Debt</a>
                                    </h4>
                                    <div class="desc">
                                        <p>We have modern facilities and amenities in reasonable prices. Moreover, the
                                            developers have also ensured to provide budget-friendly options for the
                                            residents.</p>
                                    </div>
                                    {{--                                <a href="news-details.html" class="readmore">Continure Reading</a>--}}
                                </div>
                            </div>
                        </div>
                        <!--News end-->

                        <!--News start-->
                        <div class="col">
                            <div class="news">
                                <div class="image">
                                    <a href="javascript:void(0)"><img src="assets/images/news/news-4.jpg" alt=""></a>

                                </div>
                                <div class="content">
                                    <h4 class="title"><a href="news-details.html">Bridging the home ownership gap</a>
                                    </h4>
                                    <div class="desc">
                                        <p>Many people have already invested in property village. The location, payment
                                            plan, and affordability of the property village are top selling points.</p>
                                    </div>
                                    {{--                                <a href="news-details.html" class="readmore">Continure Reading</a>--}}
                                </div>
                            </div>
                        </div>
                        <!--News end-->

                    </div>
                </div>
            </div>
        </div>
        <!--News Section end-->

        <!--Brand section start-->
        <div class="brand-section section pb-100 pb-lg-80 pb-md-70 pb-sm-60 pb-xs-50">
            <div class="container">

                <!--Section Title start-->
                <div class="row">
                    <div class="col-md-12 mb-60 mb-xs-30">
                        <div class="section-title center">
                            <h1>Our Partners</h1>
                        </div>
                    </div>
                </div>
                <!--Section Title end-->

                <div class="row">

                    <!--Brand Slider start-->
                    <div class="brand-carousel section">
                        @foreach($partners as $partner)
                            <div class="brand col"><img src="{{asset('storage/'.$partner->image)}}" alt=""></div>
                        @endforeach
                    </div>
                    <!--Brand Slider end-->

                </div>

            </div>
        </div>
        <!--Brand section end-->

        <!--Footer section start-->
        <!--Footer section end-->
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('#sub_category').attr("disabled", "disabled");
        });
    </script>
@endsection
