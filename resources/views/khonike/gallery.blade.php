@extends('khonike.layouts.layout')

@section('content')
    <!--Mobile Menu start-->
    <div class="row">
        <div class="col-12 d-flex d-lg-none">
            <div class="mobile-menu"></div>
        </div>
    </div>
    <!--Mobile Menu end-->

    </div>
    </div>
    </header>
    <!--Header section end-->

    <!--Page Banner Section start-->
    <div class="page-banner-section section"
         style="background-image: url({{asset('assets/images/bg/single-property-bg.jpg')}})">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1 class="page-banner-title">Properties</h1>
                    <ul class="page-breadcrumb">
                        <li><a href="{{route('welcome')}}">Home</a></li>
                        <li class="active">Gallery</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Page Banner Section end-->

    <!--New property section start-->

        <div
            class="property-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-100 pb-lg-80 pb-md-70 pb-sm-60 pb-xs-50">


            <div class="container">
                <div class="row">

                    <div class="col-lg-8 col-12 order-1 order-lg-2 mb-sm-50 mb-xs-50">
                        <div class="row">

                            <!--Property start-->
                            <div class="single-property col-12 mb-50">
                                <div class="property-inner">

                                    <div class="head">
                                        <div class="left">
                                            <h1 class="title">{{$property->name}}</h1>

                                            <span class="location"><img src="{{asset('assets/images/icons/marker.png')}}" alt="">{{$property->address}}</span>
                                        </div>
                                        <div class="right">
                                            <div class="type-wrap">
                                                <span class="price">PKR {{number_format($property->price)}}<span>-</span></span>
                                                <span class="type">@if($property->for_rent==1)
                                                        for Rent
                                                    @else
                                                        for sale
                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="image mb-30">
                                        @php
                                            $propertyImage = json_decode($property->images)
                                        @endphp
                                        <div class="single-property-gallery">

                                            <div class="item">
                                                <img src="{{asset('storage/'.$propertyImage[0])}}" alt="" height="350px" width="200">
                                            </div>
                                            @foreach($propertyImage as $image)

                                                <div class="item"><img src="{{asset('storage/'.$image)}}"
                                                                   alt=""height="350px" width="200"></div>

                                            @endforeach
                                        </div>

                                        <div class="single-property-thumb">
                                            @foreach($propertyImage as $image)
                                            <div class="item"><img
                                                    src="{{asset('storage/'.$image)}}" alt="" height="200px" width="200px">
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>

                                    <div class="content">

                                        <h3>Description</h3>

                                        <p>{{$property->description}}</p>

                                        <div class="row mt-30 mb-30">

                                            <div class="col-md-5 col-12 mb-xs-30">
                                                <h3>Condition</h3>
                                                <ul class="feature-list">
                                                    <li>
                                                        <div class="image"><img src="{{asset('assets/images/icons/area.png')}}"
                                                                                alt=""></div>
                                                        {{$property->area}} sqft
                                                    </li>
{{--                                                    <li>--}}
{{--                                                        <div class="image"><img src="assets/images/icons/bed.png"--}}
{{--                                                                                alt=""></div>--}}
{{--                                                        ---}}
{{--                                                    </li>--}}
{{--                                                    <li>--}}
{{--                                                        <div class="image"><img src="assets/images/icons/bath.png"--}}
{{--                                                                                alt=""></div>--}}
{{--                                                        ---}}
{{--                                                    </li>--}}
{{--                                                    <li>--}}
{{--                                                        <div class="image"><img src="assets/images/icons/parking.png"--}}
{{--                                                                                alt=""></div>--}}
{{--                                                        ---}}
{{--                                                    </li>--}}
{{--                                                    <li>--}}
{{--                                                        <div class="image"><img src="assets/images/icons/kitchen.png"--}}
{{--                                                                                alt=""></div>--}}
{{--                                                        ---}}
{{--                                                    </li>--}}
                                                </ul>
                                            </div>

{{--                                            <div class="col-md-7 col-12">--}}
{{--                                                <h3>Amenities</h3>--}}
{{--                                                <ul class="amenities-list">--}}
{{--                                                    <li>Air Conditioning</li>--}}
{{--                                                    <li>Bedding</li>--}}
{{--                                                    <li>Balcony</li>--}}
{{--                                                    <li>Cable TV</li>--}}
{{--                                                    <li>Internet</li>--}}
{{--                                                    <li>Parking</li>--}}
{{--                                                    <li>Lift</li>--}}
{{--                                                    <li>Pool</li>--}}
{{--                                                    <li>Dishwasher</li>--}}
{{--                                                    <li>Toaster</li>--}}
{{--                                                </ul>--}}
{{--                                            </div>--}}

                                        </div>

                                        <div class="row">
                                            @if($property->video_link)
                                            <div class="col-12 mb-30">
                                                <h3>Video</h3>

                                                <div class="embed-responsive embed-responsive-16by9">
                                                    <iframe class="embed-responsive-item"
                                                            src="{{$property->video_link}}"></iframe>
                                                </div>

                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Property end-->



                        </div>
                    </div>

                    <div class="col-lg-4 col-12 order-2 order-lg-1 pr-30 pr-sm-15 pr-xs-15">

                        <!--Sidebar start-->
                        <div class="sidebar">
{{--                            <h4 class="sidebar-title"><span class="text">Search Property</span><span--}}
{{--                                    class="shape"></span></h4>--}}


                            <!--Property Search start-->
{{--                            <div class="property-search sidebar-property-search">--}}

{{--                                <form action="#">--}}

{{--                                    <div>--}}
{{--                                        <input type="text" placeholder="Location">--}}
{{--                                    </div>--}}

{{--                                    <div>--}}
{{--                                        <select class="nice-select">--}}
{{--                                            <option>All Cities</option>--}}
{{--                                            <option>Athina</option>--}}
{{--                                            <option>Austin</option>--}}
{{--                                            <option>Baytown</option>--}}
{{--                                            <option>Brampton</option>--}}
{{--                                            <option>Cedar Hill</option>--}}
{{--                                            <option>Chester</option>--}}
{{--                                            <option>Chicago</option>--}}
{{--                                            <option>Coleman</option>--}}
{{--                                            <option>Corpus Christi</option>--}}
{{--                                            <option>Dallas</option>--}}
{{--                                            <option>distrito federal</option>--}}
{{--                                            <option>Fayetteville</option>--}}
{{--                                            <option>Galveston</option>--}}
{{--                                            <option>Jersey City</option>--}}
{{--                                            <option>Los Angeles</option>--}}
{{--                                            <option>Midland</option>--}}
{{--                                            <option>New York</option>--}}
{{--                                            <option>Odessa</option>--}}
{{--                                            <option>Reno</option>--}}
{{--                                            <option>San Angelo</option>--}}
{{--                                            <option>San Antonio</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}

{{--                                    <div>--}}
{{--                                        <select class="nice-select">--}}
{{--                                            <option>For Rent</option>--}}
{{--                                            <option>For Sale</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}

{{--                                    <div>--}}
{{--                                        <select class="nice-select">--}}
{{--                                            <option>Type</option>--}}
{{--                                            <option>Apartment</option>--}}
{{--                                            <option>Cafe</option>--}}
{{--                                            <option>House</option>--}}
{{--                                            <option>Restaurant</option>--}}
{{--                                            <option>Store</option>--}}
{{--                                            <option>Villa</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}

{{--                                    <div>--}}
{{--                                        <select class="nice-select">--}}
{{--                                            <option>Bedrooms</option>--}}
{{--                                            <option>1</option>--}}
{{--                                            <option>2</option>--}}
{{--                                            <option>3</option>--}}
{{--                                            <option>4</option>--}}
{{--                                            <option>5</option>--}}
{{--                                            <option>6</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}

{{--                                    <div>--}}
{{--                                        <select class="nice-select">--}}
{{--                                            <option>Bathrooms</option>--}}
{{--                                            <option>1</option>--}}
{{--                                            <option>2</option>--}}
{{--                                            <option>3</option>--}}
{{--                                            <option>4</option>--}}
{{--                                            <option>5</option>--}}
{{--                                            <option>6</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}

{{--                                    <div>--}}
{{--                                        <div id="search-price-range"></div>--}}
{{--                                    </div>--}}

{{--                                    <div>--}}
{{--                                        <button>search</button>--}}
{{--                                    </div>--}}

{{--                                </form>--}}

{{--                            </div>--}}
{{--                            <!--Property Search end-->--}}

                        </div>
                        <!--Sidebar end-->

                        <!--Sidebar start-->
                        <div class="sidebar">
                            <h4 class="sidebar-title"><span class="text">Feature Property</span><span
                                    class="shape"></span></h4>

                            @foreach($relativeProperties as $relProperty)
                                @php

                                    $propertyImage = json_decode($property->images)[0];
                                @endphp
                            <!--Sidebar Property start-->
                            <div class="sidebar-property-list mb-30">

                                <div class="sidebar-property ">
                                    <div class="image">
                                        <span class="type">@if($relProperty->for_rent==1)
                                            for Rent
                                            @else
                                            for Sale
                                            @endif
                                        </span>
                                        <a href="javascript:void(0)"><img
                                                src="{{asset('storage/'.$propertyImage)}}" alt="" height="115px" width="50px"></a>
                                    </div>
                                    <div class="content">
                                        <h5 class="title"><a href="javascript:void(0)">{{$relProperty->name}}</a>
                                        </h5>
                                        <span class="price">PKR {{number_format($relProperty->price)}}</span>
                                        <span class="location"><img src="{{asset('assets/images/icons/marker.png')}}" alt="">{{$relProperty->address}}</span>
                                    </div>
                                </div>



                            </div>
                            <!--Sidebar Property end-->
                            @endforeach
                        </div>

                        <!--Sidebar start-->
                        <div class="sidebar">
                            <h4 class="sidebar-title"><span class="text">Top Agents</span><span class="shape"></span>
                            </h4>

                            <!--Sidebar Agents start-->
                            <div class="sidebar-agent-list">

                                <div class="sidebar-agent">
                                    <div class="image">
                                        <a href="javascript:void(0)"><img src="{{asset('assets/images/ceo/ceo1.jpeg')}}"
                                                                              alt=""></a>
                                    </div>
                                    <div class="content">
                                        <h5 class="title"><a href="javascript:void(0)">Uzair Rafique</a></h5>
                                        <a href="javascript:void(0)" class="phone">0334 4440350</a>
{{--                                        <span class="properties">5 Properties</span>--}}
                                        <div class="social">
                                            <a href="https://www.facebook.com/uzair.mughal.35912" class="facebook"><i class="fa fa-facebook"></i></a>
                                            <a href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=villageproperty12345@gmail.com" target="_blank" class="google"><i class="fa fa-google-plus"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="sidebar-agent">
                                    <div class="image">
                                        <a href="javascript:void(0)"><img src="{{asset('assets/images/ceo/ceo2.jpeg')}}"
                                                                              alt=""></a>
                                    </div>
                                    <div class="content">
                                        <h5 class="title"><a href="javascript:void(0)">Hafiz Ansar Mehmood</a></h5>
                                        <a href="javascript:void(0)" class="phone">0334 5634563</a>
{{--                                        <span class="properties">5 Properties</span>--}}
                                        <div class="social">
                                            <a href="javascript:void(0)" class="facebook"><i class="fa fa-facebook"></i></a>
                                            <a href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=ansarfarooqi458@gmail.com" target="_blank" class="google"><i class="fa fa-google-plus"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="sidebar-agent">
                                    <div class="image">
                                        <a href="javascript:void(0)"><img src="{{asset('assets/images/ceo/ceo3.jpeg')}}"
                                                                              alt=""></a>
                                    </div>
                                    <div class="content">
                                        <h5 class="title"><a href="javascript:void(0)">Muhammad Shoaib</a></h5>
                                        <a href="javascript:void(0)" class="phone">0334 9650936</a>
{{--                                        <span class="properties">5 Properties</span>--}}
                                        <div class="social">
                                            <a href="javascript:void(0)" class="facebook"><i class="fa fa-facebook"></i></a>
                                            <a href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=villageproperty12345@gmail.com" target="_blank" class="google"><i class="fa fa-google-plus"></i></a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--Sidebar Agents end-->

                        </div>

                        <!--Sidebar start-->
                        <div class="sidebar">
                            <h4 class="sidebar-title"><span class="text">Popular Tags</span><span class="shape"></span>
                            </h4>

                            <!--Sidebar Tags start-->
                            <div class="sidebar-tags">
                                <a href="javascript:void(0)">Houses</a>
                                <a href="javascript:void(0)">Real Home</a>
                                <a href="javascript:void(0)">Baths</a>
                                <a href="javascript:void(0)">Beds</a>
                                <a href="javascript:void(0)">Garages</a>
                                <a href="javascript:void(0)">Family</a>
                                <a href="javascript:void(0)">Real Estates</a>
                                <a href="javascript:void(0)">Properties</a>
                                <a href="javascript:void(0)">Location</a>
                                <a href="javascript:void(0)">Price</a>
                            </div>
                            <!--Sidebar Tags end-->

                        </div>

                    </div>

                </div>
            </div>

        </div>

    <!--New property section end-->
@endsection
