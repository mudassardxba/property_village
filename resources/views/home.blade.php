{{--@extends('khonike.layouts.layout')--}}
@extends('layouts.app')
@section('content')
    <div class="content-wrapper container">

        <div class="row">
            <div class="col-lg-10 col-xs-6">
                <div class="form-group mt-30">
                    <form action="/post" method="post" enctype="multipart/form-data">
                        @csrf
                        <label class="m-2" for="title">Title</label>

                        <input type="text" name="title" class="form-control m-2">
                        <label class="m-2">Images</label>
                        <input type="file" id="input-file-now-custom-3" class="form-control m-2" name="images" multiple>
                        <button type="submit" class="btn btn-danger mt-25 ">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
