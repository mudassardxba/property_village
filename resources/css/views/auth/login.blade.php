@extends('khonike.layouts.layout')
{{--@extends('layouts.app')--}}

@section('content')

    <!--Mobile Menu start-->
    <div class="row">
        <div class="col-12 d-flex d-lg-none">
            <div class="mobile-menu"></div>
        </div>
    </div>
    <!--Mobile Menu end-->

    <!--Page Banner Section start-->
    <div class="page-banner-section section">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1 class="page-banner-title">Login or Register</h1>
                    <ul class="page-breadcrumb">
                        <li><a href="{{route('welcome')}}">Home</a></li>
                        <li class="active">Login or Register</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
{{--    <!--Page Banner Section end-->--}}

{{--    <!--Login & Register Section start-->--}}
{{--    <div class="login-register-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-100 pb-lg-80 pb-md-70 pb-sm-60 pb-xs-50">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-6 col-md-8 col-12 ml-auto mr-auto">--}}

{{--                    <ul class="login-register-tab-list nav">--}}
{{--                        <li><a class="active" href="#login-tab" data-toggle="tab">Login</a></li>--}}
{{--                        <li>or</li>--}}
{{--                        <li><a href="#register-tab" data-toggle="tab">Register</a></li>--}}
{{--                    </ul>--}}

{{--                    <div class="tab-content">--}}
{{--                        <div id="login-tab" class="tab-pane show active">--}}
{{--                            <form action="#">--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-12 mb-30"><input type="text" placeholder="User Name" required></div>--}}
{{--                                    <div class="col-12 mb-30"><input type="password" placeholder="Password" required></div>--}}
{{--                                    <div class="col-12 mb-30">--}}
{{--                                        <ul>--}}
{{--                                            <li><input type="checkbox" id="login_remember" required> <label for="login_remember">Remember me</label></li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-12 mb-30"><button class="btn">Login</button></div>--}}

{{--                                    <div class="col-12 d-flex justify-content-between">--}}
{{--                                        <span>New User to Khonike?&nbsp; <a class="register-toggle" href="#register-tab">Register!</a></span>--}}
{{--                                        <span><a href="forgot-password.html">Forgot Password ?</a></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </form>--}}
{{--                        </div>--}}
{{--                        <div id="register-tab" class="tab-pane">--}}
{{--                            <form action="#">--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-12 mb-30"><input type="text" placeholder="First Name" required></div>--}}
{{--                                    <div class="col-12 mb-30"><input type="text" placeholder="Last Name" required></div>--}}
{{--                                    <div class="col-12 mb-30"><input type="email" placeholder="Email" required></div>--}}
{{--                                    <div class="col-12 mb-30"><input type="text" placeholder="User Name" required></div>--}}
{{--                                    <div class="col-12 mb-30"><input type="password" placeholder="Password" required></div>--}}
{{--                                    <div class="col-12 mb-30"><input type="password" placeholder="Confirm Password" required></div>--}}
{{--                                    <div class="col-12 mb-30">--}}
{{--                                        <ul>--}}
{{--                                            <li><input type="checkbox" id="register_agree" required><label for="register_agree">I agree with your <a href="#">Terms & Conditions</a></label></li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-12 mb-30">--}}
{{--                                        <ul>--}}
{{--                                            <li><input type="radio" name="account_type" id="register_normal" checked><label for="register_normal">Normal</label></li>--}}
{{--                                            <li><input type="radio" name="account_type" id="register_agent"><label for="register_agent">Agent</label></li>--}}
{{--                                            <li><input type="radio" name="account_type" id="register_agency"><label for="register_agency">Agency</label></li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-12"><button class="btn">Register</button></div>--}}
{{--                                </div>--}}
{{--                            </form>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <!--Login & Register Section end-->

    <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 mt-50 mb-50">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                        <br>
                        <div class="col-12 d-flex justify-content-between ">
                                       <span>New User to Khonike?&nbsp; <a  href="{{route('register')}}">Register!</a></span>
{{--                                        <span><a href="forgot-password.html">Forgot Password ?</a></span>--}}
                                   </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
