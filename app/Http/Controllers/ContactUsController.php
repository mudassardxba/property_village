<?php

namespace App\Http\Controllers;

use App\Models\CompanySetting;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function index()
    {
        $companySetting=CompanySetting::all();
        $emails= $companySetting->where('key','email')->pluck('detail');
        return view('khonike.contact',compact('emails'));
    }
}
