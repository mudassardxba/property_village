<?php

namespace App\Http\Controllers;

use App\Models\CategoryType;
use App\Models\City;
use App\Models\Partner;
use App\Models\Property;
use App\Models\SubCategory;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $types=CategoryType::all();
        $cities=City::all();
        $partners = Partner::all();
        $properties = Property::paginate(4);
        $subCategory = SubCategory::all();
        return view('khonike.welcome',compact('types','cities','partners','properties','subCategory'));
    }

    public function property(Request $request)
    {

       $categoryTypeId = CategoryType::where('id',$request->type)->pluck('id')->first();
       $cityId = City::where('id',$request->city)->pluck('id')->first();
         $subCategoryId = SubCategory::where('id',$request->title)->pluck('id')->first();
        $propertyQuery = Property::query();

        if(!empty($categoryTypeId)){
            $propertyQuery = $propertyQuery->where('category_types_id',$categoryTypeId);
        }elseif(!empty($cityId)){
            $propertyQuery = $propertyQuery->where('city_id',$cityId);
        }else{
            $propertyQuery = $propertyQuery->where('sub_category_id',$subCategoryId);
        }

        $forRent = $request->for_rent ? 1: 0;

        $properties = $propertyQuery->where('for_rent',$forRent)->get();


        $types=CategoryType::all();
        $cities=City::all();
        $partners = Partner::all();
        $subCategory = SubCategory::all();

        $searchedArray = $request->all();

        return view('khonike.welcome',compact('types','cities','partners','properties' , 'searchedArray','subCategory'));
    }
//    public function City()
//    {
//        $cities=City::all();
//        return view('khonike.welcome',compact('cities'));
//    }
}
