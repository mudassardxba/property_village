<?php

namespace App\Http\Controllers;

use App\Models\Property;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function index($id)
    {
        $property= Property::find($id);
        $relativeProperties = Property::where('category_types_id',$property->category_types_id)->limit(3)->get();
        return view('khonike.gallery',compact('property','relativeProperties'));
    }
}
